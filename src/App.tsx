"use strict";

import * as React from "react";

import Button from "antd/lib/button";
import Form from "antd/lib/form";
import Table from "antd/lib/table";

import columns from "./components/Columns";

import "./App.css";

import logo from "./rocket.svg";

// import * as debug from "debug";

interface IPaper {
  author: string;
  title: string;
  id: number | string;
}

class App extends React.Component<{}, { count: number; paperData: IPaper[] }> {
  // public buttonDebugger = debug("app-button");

  constructor(props: any) {
    super(props);
    this.state = { count: 0, paperData: [] };
  }

  public componentWillMount() {
    fetch("http://localhost:3000/papers", { method: "GET" })
      .then(response => response.json())
      .then(jsonResponse => this.setState({ paperData: jsonResponse }))
      .catch(error => {
        return error;
      });
  }

  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Paper Rocket</h1>
        </header>
        <p className="App-intro">
          To get started, click the button on the bottom to add a paper.
        </p>
        <Form />
        <Table columns={columns} dataSource={this.state.paperData} />
        <Button type="primary" htmlType="button" onClick={this.onButtonClicked}>
          Add
        </Button>
        <p>
          The button has been clicked {this.state.count}{" "}
          {this.state.count === 1 ? "time" : "times"}.
        </p>
      </div>
    );
  }

  public onButtonClicked = () => {
    this.setState({ count: this.state.count < 10 ? this.state.count + 1 : 0 });
    // this.buttonDebugger("button clicked");
    // alert("Button Clicked!");
  };
}

export default App;
