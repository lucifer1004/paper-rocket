import * as React from "react";

import Divider from "antd/lib/divider";
import Icon from "antd/lib/icon";

let columns: any[];

export default (columns = [
  {
    dataIndex: "author",
    key: "author",
    render: (text: string) => <a href="javascript:;">{text}</a>,
    title: "Author"
  },
  {
    dataIndex: "title",
    key: "title",
    title: "Title"
  },
  {
    key: "action",
    render: (text: string, record: any) => (
      <span>
        <a href="javascript:;">Edit</a>
        <Divider type="vertical" />
        <a href="javascript:;">Delete</a>
        <Divider type="vertical" />
        <a href="javascript:;" className="ant-dropdown-link">
          More actions <Icon type="down" />
        </a>
      </span>
    ),
    title: "Action"
  }
]);
